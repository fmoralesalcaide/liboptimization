#include "Lib/Optimization/Solution/Solution.h"

namespace Lib
{
    namespace Optimization
    {
        Solution::~Solution()
        {}

        const Solution::Type&
        Solution::getType() const
        {
            return type;
        }

        const cost_t&
        Solution::getCost() const
        {
            return cost;
        }

        void
        Solution::updateCost()
        {
            cost = recomputeCost();
        }

        Solution::Solution(const Solution::Type& type)
        : cost(MAX_COST),
          type(type)
        {}

        cost_t
        Solution::recomputeCost()
        {
            throw std::runtime_error("Not implemented");
        }
    }
}
