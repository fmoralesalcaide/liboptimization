#pragma once

#include "Lib/Optimization/Generic/GenericDefinitions.h"

namespace Lib
{
    namespace Optimization
    {
        class Candidate
        {
            public:
                virtual ~Candidate();

                // Is the candidate implemented
                const bool& isImplemented() const;

                // Get (last computed) candidate cost
                const cost_t& getCost() const;

                // Has the candidate a constant cost?
                const bool& isConstantCost() const;

                // Update candidate cost based on the solution
                void updateCost(CandidateBasedSolution* solution);

                // Implements this candidate in the solution
                void implementCandidate(CandidateBasedSolution* solution);

            protected:
                Candidate(const bool& hasConstantCost = false, const cost_t& constantCost = MAX_COST);

                // Implements this candidate in the solution
                virtual void implement(CandidateBasedSolution* solution) = 0;

                // Recompute candidate cost
                virtual cost_t recomputeCost(CandidateBasedSolution* solution) = 0;

            private:
                bool implemented;
                bool hasConstantCost;
                bool costComputed;
                cost_t cost;
        };
    }
}
