#include "Lib/Optimization/Solution/CandidateBased/CandidateBasedSolution.h"
#include "Lib/Optimization/Solution/CandidateBased/Candidate.h"

namespace Lib
{
    namespace Optimization
    {
        CandidateBasedSolution::~CandidateBasedSolution()
        {
            for(Candidate* candidate : candidates)
                delete candidate;
        }

        void
        CandidateBasedSolution::addCandidate(Candidate* candidate)
        {
            candidates.push_back(candidate);
            candidate->implementCandidate(this);
        }

        const std::vector<Candidate*>&
        CandidateBasedSolution::getCandidates() const
        {
            return candidates;
        }

        CandidateBasedSolution::CandidateBasedSolution()
        : Solution(Solution::Type::CANDIDATE_BASED)
        {}
    }
}
