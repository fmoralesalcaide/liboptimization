#include "Lib/Optimization/Solution/CandidateBased/Candidate.h"
#include "Lib/Optimization/Solution/CandidateBased/CandidateBasedSolution.h"

namespace Lib
{
    namespace Optimization
    {
        Candidate::~Candidate()
        {}

        const bool&
		Candidate::isImplemented() const
        {
        	return implemented;
        }

        const cost_t&
        Candidate::getCost() const
        {
            if(!costComputed)
                throw std::runtime_error("Candidate cost not computed");
            return cost;
        }

        const bool&
		Candidate::isConstantCost() const
        {
        	return hasConstantCost;
        }

        void
        Candidate::updateCost(CandidateBasedSolution* solution)
        {
        	if(hasConstantCost)
        		return;
            cost = recomputeCost(solution);
            costComputed = true;
        }

        void
		Candidate::implementCandidate(CandidateBasedSolution* solution)
        {
        	if(implemented)
        		throw std::runtime_error("Candidate already implemented");
        	implement(solution);
        	implemented = true;
        }

        Candidate::Candidate(const bool& hasConstantCost, const cost_t& constantCost)
        : implemented(false),
		  hasConstantCost(hasConstantCost),
		  costComputed(hasConstantCost),
          cost(constantCost)
        {}
    }
}
