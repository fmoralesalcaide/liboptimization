#pragma once

#include "Lib/Optimization/Solution/Solution.h"

namespace Lib
{
    namespace Optimization
    {
        class CandidateBasedSolution : public Solution
        {
            public:
                virtual ~CandidateBasedSolution();

                // Generates the candidate list (CL)
                virtual std::vector<Candidate*> computeCandidateList() = 0;

                // Add a candidate to the solution
                void addCandidate(Candidate* candidate);

                // Return the candidates (ordered by selection)
                const std::vector<Candidate*>& getCandidates() const;

            protected:
                std::vector<Candidate*> candidates;

                CandidateBasedSolution();
        };
    }
}

