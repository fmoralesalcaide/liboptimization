#pragma once

#include "Lib/Optimization/Generic/GenericDefinitions.h"

namespace Lib
{
    namespace Optimization
    {
        class Solution
        {
            public:
                enum class Type
                {
                    GENERIC,
                    CANDIDATE_BASED
                };

                virtual ~Solution();

                // Return solution type
                const Type& getType() const;

                // Return the current solution cost
                const cost_t& getCost() const;

                // Update solution cost
                void updateCost();

                // Copy this instance
                virtual Solution* clone() = 0;

            protected:
                cost_t cost;
                Type type;

                Solution(const Type& type);

                // Recompute solution cost
                virtual cost_t recomputeCost();

        };
    }
}

