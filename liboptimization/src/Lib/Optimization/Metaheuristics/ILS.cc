#include "Lib/Optimization/Metaheuristics/ILS.h"
#include "Lib/Optimization/Problem.h"

namespace Lib
{
    namespace Optimization
    {
        namespace Metaheuristics
        {
            ILS::ILS(const double& strength, const unsigned int& maxIterations, const double& maxRunningTimeMs, const unsigned int& randomSeed)
            : Metaheuristic(maxIterations, maxRunningTimeMs, randomSeed),
              strength(strength),
              iteratedSolution(nullptr)
            {}

            ILS::~ILS()
            {
                if(iteratedSolution)
                    delete iteratedSolution;
            }

            void
            ILS::setInitialSolution(Solution* initialSolution)
            {
                if(iteratedSolution)
                    throw std::runtime_error("Initial solution already set");
                if(!initialSolution)
                    throw std::runtime_error("Cannot set NULL initial solution");
                iteratedSolution = initialSolution;
            }

            void
            ILS::run()
            {
                if(!iteratedSolution)
                {
                	TimePoint iterationStartTime = std::chrono::high_resolution_clock::now();

                    iteratedSolution = problem->instantiateSolution();
                    constructivePhase(iteratedSolution);

                    TimePoint localSearchStartTime = std::chrono::high_resolution_clock::now();

					localSearchPhase(iteratedSolution);

					TimePoint localSearchEndTime = std::chrono::high_resolution_clock::now();
					executionInfo.avgLocalSearchTime += std::chrono::duration_cast<std::chrono::milliseconds>(localSearchEndTime - localSearchStartTime).count();

                    TimePoint iterationEndTime = std::chrono::high_resolution_clock::now();
                    executionInfo.avgSolutionTime += std::chrono::duration_cast<std::chrono::milliseconds>(iterationEndTime - iterationStartTime).count();
                    executionInfo.numSolutions++;
                }
                updateSolution(iteratedSolution);

                while(!isStoppingCriterionMet())
                {
                	TimePoint iterationStartTime = std::chrono::high_resolution_clock::now();

                    applyPerturbation(iteratedSolution);

                    TimePoint localSearchStartTime = std::chrono::high_resolution_clock::now();

                    localSearchPhase(iteratedSolution);

                    TimePoint localSearchEndTime = std::chrono::high_resolution_clock::now();
                    executionInfo.avgLocalSearchTime += std::chrono::duration_cast<std::chrono::milliseconds>(localSearchEndTime - localSearchStartTime).count();

                    TimePoint iterationEndTime = std::chrono::high_resolution_clock::now();
                    executionInfo.avgSolutionTime += std::chrono::duration_cast<std::chrono::milliseconds>(iterationEndTime - iterationStartTime).count();
                    executionInfo.numSolutions++;

                    updateSolution(iteratedSolution);
                }
            }
        }
    }
}


