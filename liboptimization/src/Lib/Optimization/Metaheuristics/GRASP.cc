#include "Lib/Optimization/Metaheuristics/Metaheuristic.h"
#include "Lib/Optimization/Metaheuristics/GRASP.h"
#include "Lib/Optimization/Problem.h"
#include "Lib/Optimization/Solution/CandidateBased/CandidateBasedSolution.h"
#include "Lib/Optimization/Solution/CandidateBased/Candidate.h"

namespace Lib
{
    namespace Optimization
    {
    	namespace Metaheuristics
		{
			GRASP::GRASP(const double& alpha, const unsigned int& maxIterations, const double& maxRunningTimeMs, const unsigned int& randomSeed)
            : Metaheuristic(maxIterations, maxRunningTimeMs, randomSeed),
			  alpha(alpha)
			{
				if(alpha < 0 || alpha > 1)
					throw std::runtime_error("Invalid parameter alpha");
			}

			GRASP::~GRASP()
			{}

			void
			GRASP::run()
			{
				while(!isStoppingCriterionMet())
				{
				    Solution* _currentSolution = problem->instantiateSolution(Solution::Type::CANDIDATE_BASED);
					CandidateBasedSolution* currentSolution = (CandidateBasedSolution*) _currentSolution;

					constructivePhase(currentSolution);
					localSearchPhase(currentSolution);
					updateSolution(currentSolution);

					delete currentSolution;
				}
			}

			void
			GRASP::constructivePhase(CandidateBasedSolution* currentSolution)
			{
				struct CandidateCostComparator
				{
					inline bool operator() (Candidate* lhs, Candidate* rhs)
					{
					    return lhs->getCost() < rhs->getCost();
					}
				};

				std::vector<Candidate*> CL = currentSolution->computeCandidateList();

				// Optimize GRASP if the candidate costs are constant
				bool constantCosts = true;

				for(Candidate* candidate : CL)
				{
					if(!candidate->isConstantCost())
					{
						constantCosts = false;
						break;
					}
				}

				if(constantCosts)
					std::stable_sort(CL.begin(), CL.end(), CandidateCostComparator());

				while(!CL.empty())
				{
					// Update candidate costs
					if(!constantCosts)
					{
						for(Candidate* candidate : CL)
							candidate->updateCost(currentSolution);

						// Sort candidate list (CL)
						std::stable_sort(CL.begin(), CL.end(), CandidateCostComparator());
					}

					double lb = CL.front()->getCost();
					double ub = CL.back()->getCost();
					double threshold = 0;

					// Finish if no feasible candidate is available
					if(lb == MAX_COST)
						break;

					if(CL.size() == 1)
						threshold = ub;
					else
					{
						if(alpha > 0 && ub == MAX_COST)
						{
							// Look for the highest feasible candidate
							unsigned int i = CL.size() - 1;
							while(ub == MAX_COST)
								ub = CL.at(--i)->getCost();
						}
						threshold = alpha*ub + (1-alpha)*lb;
					}

					// Build restricted candidate list (RCL)
					std::vector<unsigned int> RCL;
					for(unsigned int i = 0; i < CL.size(); ++i)
					{
						if(CL.at(i)->getCost() > threshold)
							break;
						RCL.push_back(i);
					}

					// Select an element from the RCL at random
					std::shuffle(RCL.begin(), RCL.end(), randNumberGenerator);
					unsigned int i = RCL.at(0);
					Candidate* selectedCandidate = CL.at(i);

					currentSolution->addCandidate(selectedCandidate);
					CL.erase(CL.begin() + i);
				}

				for(Candidate* unselectedCandidate : CL)
					delete unselectedCandidate;
			}

			void
			GRASP::localSearchPhase(CandidateBasedSolution*)
			{}
		}
    }
}
