#include "Lib/Optimization/Metaheuristics/Metaheuristic.h"
#include "Lib/Optimization/Solution/Solution.h"
#include "Lib/Optimization/Problem.h"

namespace Lib
{
    namespace Optimization
    {
        namespace Metaheuristics
        {
            Metaheuristic::~Metaheuristic()
            {}

            void
            Metaheuristic::solve(Problem* newProblem)
            {
                if(bound)
                    throw std::runtime_error("Meta-heuristic instance is already bound to a problem (instantiate a new one)");
                if(!newProblem)
                    throw std::runtime_error("ILS cannot solve a NULL problem");

                bound = true;
                problem = newProblem;

                startingTime = std::chrono::high_resolution_clock::now();
                run();
                endingTime = std::chrono::high_resolution_clock::now();

                runningTimeMs = std::chrono::duration_cast<std::chrono::milliseconds>(endingTime - startingTime).count();
                executionInfo.totalRunningTime = runningTimeMs;
            }

            const ExecutionInfo&
			Metaheuristic::getExecutionInfo() const
            {
            	return executionInfo;
            }

            const std::vector<SolutionUpdateInfo>&
            Metaheuristic::getSolutionUpdates() const
            {
                return solutionUpdates;
            }

            bool
            Metaheuristic::isStoppingCriterionMet()
            {
                // Evaluate iterations
                if(currentIteration++ == maxIterations)
                    return true;

                // Evaluate running time
                TimePoint currentTime = std::chrono::high_resolution_clock::now();
                double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startingTime).count();
                if(elapsed >= maxRunningTimeMs)
                    return true;

                return false;
            }

            void
            Metaheuristic::updateSolution(Solution* solution)
            {
                solution->updateCost();

                if(problem->improvesSolution(solution))
                {
                    problem->setSolution(solution);

                    // Update statistics about the solution evolution
                    SolutionUpdateInfo solutionUpdate;

                    TimePoint currentTime = std::chrono::high_resolution_clock::now();
                    solutionUpdate.elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startingTime).count();
                    solutionUpdate.iteration = currentIteration;
                    solutionUpdate.cost = solution->getCost();

                    if(solutionUpdates.empty())
                    {
                        executionInfo.firstSolutionTime = solutionUpdate.elapsedTime;
                        executionInfo.firstSolutionCost = solutionUpdate.cost;
                    }

                    executionInfo.bestSolutionTime = solutionUpdate.elapsedTime;
                    executionInfo.bestSolutionCost = solutionUpdate.cost;
                    executionInfo.bestSolutionIter = executionInfo.numSolutions - 1;
                    executionInfo.numImprovements++;

                    solutionUpdates.push_back(solutionUpdate);
                }
            }

            Metaheuristic::Metaheuristic(const unsigned int& maxIterations, const double& maxRunningTimeMs, const unsigned int& randomSeed)
            : problem(nullptr),
              maxIterations(maxIterations),
              currentIteration(0),
              randomSeed(randomSeed),
              randNumberGenerator(randomSeed),
              maxRunningTimeMs(maxRunningTimeMs),
              runningTimeMs(0),
              bound(false)
            {}
        }
    }
}
