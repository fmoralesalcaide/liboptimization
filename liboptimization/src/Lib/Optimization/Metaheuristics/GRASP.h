/*
 * Name: Greedy Randomized Adaptive Search Procedure (GRASP)
 * Authors: T. Feo & M. Resende
 * Source: "Greedy Randomized Adaptive Search Procedures," Journal of
 * global optimization, 1995.
 *
 * */

#pragma once

#include "Lib/Optimization/Metaheuristics/Metaheuristic.h"

namespace Lib
{
    namespace Optimization
    {
        namespace Metaheuristics
        {
            class GRASP : public Metaheuristic
            {
                public:
                    GRASP(const double& alpha, const unsigned int& maxIterations, const double& maxRunningTimeMs, const unsigned int& randomSeed);
                    virtual ~GRASP();

                protected:
                    double alpha;

                    void run();
                    virtual void localSearchPhase(CandidateBasedSolution* currentSolution);

                private:
                    virtual void constructivePhase(CandidateBasedSolution* currentSolution);
            };
        }
    }
}

