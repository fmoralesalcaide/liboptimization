/*
 * Name: Iterated Local Search (ILS)
 * Authors: H. Lourenço, O. Martin & T. Stülze
 * Source: "Iterated Local Search," Handbook of Metaheuristics, Chapter 11, 2003.
 *
 * */

#pragma once

#include "Lib/Optimization/Metaheuristics/Metaheuristic.h"

namespace Lib
{
    namespace Optimization
    {
        namespace Metaheuristics
        {
            class ILS : public Metaheuristic
            {
                public:
                    ILS(const double& strength, const unsigned int& maxIterations, const double& maxRunningTimeMs, const unsigned int& randomSeed);
                    virtual ~ILS();

                    void setInitialSolution(Solution* initialSolution);

                protected:
                    double strength;
                    Solution* iteratedSolution;

                    void run();

                    virtual void constructivePhase(Solution* solution) = 0;
                    virtual void applyPerturbation(Solution* solution) = 0;
                    virtual void localSearchPhase(Solution* solution)  = 0;
            };
        }
    }
}
