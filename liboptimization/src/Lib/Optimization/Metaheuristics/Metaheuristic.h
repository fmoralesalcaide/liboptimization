#pragma once

#include "Lib/Optimization/Generic/GenericDefinitions.h"

namespace Lib
{
    namespace Optimization
    {
        namespace Metaheuristics
        {
            class Metaheuristic
            {
                public:
                    virtual ~Metaheuristic();

                    void solve(Problem* newProblem);

                    const ExecutionInfo& getExecutionInfo() const;
                    const std::vector<SolutionUpdateInfo>& getSolutionUpdates() const;


                protected:
                    Problem* problem;

                    unsigned int maxIterations;
                    unsigned int currentIteration;

                    unsigned int randomSeed;
                    std::mt19937 randNumberGenerator;

                    double maxRunningTimeMs;
                    double runningTimeMs;
                    TimePoint startingTime;
                    TimePoint endingTime;

                    ExecutionInfo executionInfo;
                    std::vector<SolutionUpdateInfo> solutionUpdates;

                    bool bound;

                    virtual bool isStoppingCriterionMet();
                    virtual void updateSolution(Solution* solution);
                    virtual void run() = 0;

                    Metaheuristic(const unsigned int& maxIterations, const double& maxRunningTimeMs, const unsigned int& randomSeed);
            };
        }
    }
}
