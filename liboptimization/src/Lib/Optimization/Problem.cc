#include "Lib/Optimization/Problem.h"
#include "Lib/Optimization/Solution/Solution.h"

namespace Lib
{
	namespace Optimization
	{
		Problem::~Problem()
		{
			if(solution)
				delete solution;
		}

        Solution*
        Problem::getSolution() const
        {
            if(!solution)
                throw std::runtime_error("Problem has not a solution");
            return solution;
        }

        bool
        Problem::improvesSolution(Solution* newSolution)
        {
            if(!newSolution)
                throw std::runtime_error("Cannot compare a NULL solution");
            if(!solution)
                return true;
            return newSolution->getCost() < solution->getCost();
        }

        void
        Problem::setSolution(Solution* newSolution)
        {
            if(!newSolution)
                throw std::runtime_error("Cannot set a NULL solution for a problem");
            if(solution)
                delete solution;
            solution = newSolution->clone();
        }

        Problem::Problem()
        : solution(nullptr)
        {}
	}
}
