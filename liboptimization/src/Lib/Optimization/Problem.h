#pragma once

#include "Lib/Optimization/Solution/Solution.h"

namespace Lib
{
	namespace Optimization
	{
		class Problem
		{
			public:
				virtual ~Problem();

				// Returns the problem solution
				Solution* getSolution() const;

				// Check if a solution improves the current one
				virtual bool improvesSolution(Solution* newSolution);

				// Set/Replace the current solution
				void setSolution(Solution* newSolution);

				// Instantiates an new, empty solution
				virtual Solution* instantiateSolution(const Solution::Type& type = Solution::Type::GENERIC) = 0;

			protected:
                Problem();

			private:
				Solution* solution;
		};
	}
}

