#pragma once

/*######################################*/
/*#             STL includes           #*/
/*######################################*/

#include <limits>
#include <vector>
#include <string>
#include <chrono>
#include <random>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <type_traits>
#include <unordered_map>

/*######################################*/
/*#           Defines, Typedefs        #*/
/*######################################*/

typedef std::chrono::high_resolution_clock::time_point TimePoint;

typedef double cost_t;
#define MAX_COST std::numeric_limits<cost_t>::max()
#define MIN_COST std::numeric_limits<cost_t>::min()

/*######################################*/
/*#         Forward declarations       #*/
/*######################################*/

namespace Lib
{
    namespace Optimization
    {
        class Problem;
        class Solution;

        class CandidateBasedSolution;
        class Candidate;

        namespace Metaheuristics
        {
            class Metaheuristic;
            class GRASP;
            class ILS;
        }
    }
}

/*######################################*/
/*#              Structs               #*/
/*######################################*/

namespace Lib
{
    namespace Optimization
    {
        struct SolutionUpdateInfo
        {
            cost_t cost;
            unsigned int iteration;
            double elapsedTime;
        };
    }
}

namespace Lib
{
    namespace Optimization
    {
        struct ExecutionInfo
        {
        	// First and second phase info
        	double timeFirstPhase;
        	unsigned int processedODFirstPhase;

        	double timeSecondPhase;
        	unsigned int processedODSecondPhase;

        	// Third phase info
            double firstSolutionTime;
            cost_t firstSolutionCost;

            double bestSolutionTime;
            cost_t bestSolutionCost;
            unsigned int bestSolutionIter;

            double totalRunningTime;
            double avgSolutionTime;
            double avgLocalSearchTime;
            double relLocalSearchTime;

            unsigned int numSolutions;
            unsigned int numImprovements;

            double avgShortestPathTime;
            unsigned int numShortestPaths;

            ExecutionInfo()
            : processedODFirstPhase(0),
			  processedODSecondPhase(0),
			  bestSolutionIter(0),
			  avgSolutionTime(0),
			  avgLocalSearchTime(0),
			  numSolutions(0),
			  numImprovements(0),
			  avgShortestPathTime(0),
			  numShortestPaths(0)
            {}

            void finalize()
            {
            	avgSolutionTime /= double(numSolutions);
            	avgLocalSearchTime /= double(numSolutions);
            	relLocalSearchTime = avgLocalSearchTime / avgSolutionTime;
            	avgShortestPathTime /= double(numShortestPaths);
            }
        };
    }
}

/*######################################*/
/*#               Utils                #*/
/*######################################*/

namespace Lib
{
    namespace Optimization
    {
        std::string printSolutionUpdates(const std::vector<SolutionUpdateInfo>& solutionUpdates);
        std::string printSolutionUpdate(const SolutionUpdateInfo& solutionUpdate);
    }
}
