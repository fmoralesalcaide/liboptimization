#include "Lib/Optimization/Generic/GenericDefinitions.h"

/*######################################*/
/*#               Utils                #*/
/*######################################*/

namespace Lib
{
    namespace Optimization
    {
        std::string printSolutionUpdates(const std::vector<SolutionUpdateInfo>& solutionUpdates)
        {
            std::ostringstream oss;
            oss << "Time\tIter\tCost" << std::endl;

            for(unsigned int i = 0; i < solutionUpdates.size(); ++i)
                oss << printSolutionUpdate(solutionUpdates.at(i)) << std::endl;

            if(!solutionUpdates.empty())
            {
                const Lib::Optimization::SolutionUpdateInfo& lastUpdate = solutionUpdates.back();
                oss << "Average iteration time: " << lastUpdate.elapsedTime / (1 + lastUpdate.iteration) << " ms" << std::endl;
            }
            return oss.str();
        }

        std::string printSolutionUpdate(const SolutionUpdateInfo& solutionUpdate)
        {
            std::ostringstream oss;
            oss << std::fixed;
            oss << std::setprecision(3);
            oss << solutionUpdate.elapsedTime << ";";
            oss << solutionUpdate.iteration << ";";
            oss << solutionUpdate.cost;
            return oss.str();
        }
    }
}
