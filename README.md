# README #

Liboptimization is a C++ library providing different metaheuristic implementations.

### What metaheuristics are implemented? ###

The currently implemented metaheuristics are:

* Greedy randomized adaptive search procedure (GRASP).
* Iterated local search (ILS).